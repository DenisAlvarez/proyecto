<?php defined('BASEPATH') OR exit('No direct script access allowed');


/*
* Lenguaje modulos
*/

$lang['create_permissin'] = 'Crear permisos';
$lang['title_create_rol'] =  'Crear permisos';
$lang['title_update_rol'] =  'Editar permisos';
$lang['permissin_asign'] = 'Listado de modulos';
$lang['success_insert_rol'] = 'Permisos registrados exitosamente';
$lang['success_update_rol'] = 'Permisos actualizados exitosamente';
$lang['return_list_rol'] = 'Volver al listado de permisos';
