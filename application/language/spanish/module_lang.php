<?php defined('BASEPATH') OR exit('No direct script access allowed');


/*
* Lenguaje modulos
*/

$lang['create_module'] = 'Registrar modulo';
$lang['title_mod_create_module'] = 'Registro de modulo';
$lang['title_update_mod'] = 'Editar modulo';
$lang['success_insert_mod'] = 'Modulo registrado exitosamente';
$lang['success_update_mod'] = 'Modulo actualizado exitosamente';
$lang['return_list_mod'] = 'Volver al listado de modulos';
$lang['mod_parent'] = 'Modulo padre';
$lang['mod_name'] = 'Nombre del modulo';
$lang['mod_description'] = 'Descripcion del modulo';
$lang['mod_slug'] = 'PATH (URL del modulo)';
