<?php

class Empleados_model extends CI_Model
{

	/*
	*
	*
	*
	*/

	public function __construct()
	{

		parent::__construct();

		$this->lang->load('employes');

	}


	public function createEmpleado($data_employe)
	{
		$data_employe = json_decode($data_employe); //Decodifica el string que le llega del controlador

		//se asigana a variables lo que se recibe
		$cedula = $data_employe->cedula;
		$nombre  = $data_employe->nombre;
		$apellidos = $data_employe->apellidos;
		$telef = $data_employe->telef;
		$correo = $data_employe->correo;
		$direccion = $data_employe->direccion;
		$observ = $data_employe->observ;
		
		
		$this->db->trans_start(); //inicia la transacción .... 
		$sql = "INSERT INTO mp_empleados (tbl_emple_cedula, tbl_emple_nombres, tbl_emple_apellidos, tbl_emple_telef, tbl_emple_correo, tbl_emple_direccion, tbl_emple_observ) VALUES
		('".$cedula."', '".$nombre."', '".$apellidos."', '".$telef."', '".$correo."', '".$direccion."', '".$observ."')";
		$query = $this->db->query($sql);
		$ep_id = $this->db->insert_id(); //ultimo id insertado
		$this->db->trans_complete();

		


		if($this->db->trans_status() === TRUE)
		{
		
			$response = array(
			'respon_code' => 1,
			'ep_id' => $ep_id,
			'respon_msg' => 'Se registro con éxito la información . . .',
		);

	} else {

		$response = array(
			'respon_code' => 0,
			//'respon_msg' => lang('not_insert_employe') // Esto me da error con la function lang()
		);

	}

		return json_encode($response);


	} //Fin de la function createEmpleado




}