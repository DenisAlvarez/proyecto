function validate_employes(){

    var regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var regex_name = /^[A-Za-zÑñáéíóúÁÉÍÓÚ ]{2,50}$/;
    var regex_identitycard = /^[0-9]{7,15}$/;
    var regex_phone = /^[0-9]{11,20}$/;
    var regex_code_fiscal = /^[0-9]{1,2}$/;
    var regex_address = /^[A-Za-zÑñáéíóúÁÉÍÓÚ0-9., ]{10,255}$/;

      jQuery.validator.addMethod("input", function(value, element){

      var testing;
      switch (element.id){
        case 'identity_card':
          testing = regex_identitycard;
          break;
        case 'identity_fiscal':
          testing = regex_identitycard;
          break;
        case 'code_fiscal':
          testing = regex_code_fiscal;
          break;
        case 'firstname':
          testing = regex_name;
          break;
        case 'lastname':
          testing = regex_name;
          break;
        case 'lastfirstname':
          testing = regex_name;
          break;
        case 'lastlastname':
          testing = regex_name;
          break;
        case 'phone_mobile':
          testing = regex_phone;
          break;
        case 'phone_fixed':
          testing = regex_phone;
          break;
        case 'email':
          testing = regex_email;
          break;
        case 'direction':
          testing = regex_address;
          break;
      }
      return testing.test(value);
    });

    jQuery('#form_employe').validate({

      errorClass: 'error',
      validClass: 'success',

      rules:{
        identity_card: {required:true, input:true},
        identity_fiscal: {required:true, input:true},
        code_fiscal: {required:true, input:true},
        firstname: {required:true, input:true},
        lastfirstname: {required:true, input:true},
        datebirth: {required:true},
        departament: {required:true},
        position: {required:true},
        dateentry:{required:true},
        phone_mobile: {required:true, input:true},
        phone_fixed: {required:true, input:true},
        email: {required:true, input:true},
        direction: {required:true, input:true}
      },

      messages: {
          identity_card:{
            required: 'El numero de identificacion legal es obligatorio.',
            input: 'El campo solo admite numeros, minimo 7 maximo 15 caracteres.'
          },
          identity_fiscal: {
            required: 'El numero de identificacion fiscal es obligatorio.',
            input: 'Solo admite numeros minimo 7, maximo 15 caracteres.'
          },
          code_fiscal: {
            required: 'El codigo del numero fiscal es obligatorio.',
            input: 'Solo admite numeros 1 caracteres.'
          },
          firstname: {
            required: 'El campo primer nombre es obligatorio.',
            input: 'Solo admite letras minimo 2, maximo 50 caracteres.'
          },
          lastfirstname: {
            required: 'El campo primer apellido es obligatorio.',
            input: 'Solo admite letras minimo 2, maximo 50 caracteres.'
          },
          datebirth: {
            required: 'El campo Fecha de nacimiento es obligatorio.',
            input: 'Solo admite un formato de fecha valido dd-mm-yyyy.'
          },
          departament: {
            required: 'El campo departamento es obligatorio.'
          },
          position: {
            required: 'El campo Cargo es obligatorio.'
          },
          dateentry: {
            required: 'El campo Fecha de ingreso es obligatorio.'
          },
          phone_mobile: {
            required: 'El campo Telefono movil es obligatorio.',
            input: 'Solo admite numeros minimo 11, maximo 20 caracteres.'
          },
          phone_fixed: {
            required: 'El campo Telefono fijo es obligatorio.',
            input: 'Solo admite numeros minimo 11, maximo 20 caracteres.'
          },
          email: {
            required: 'El campo correo electrónico es obligatorio.',
            input: 'Debe ingresar una dirección de correo valida.'
          },
          direction: {
            required: 'El campo Direccion es obligatorio.',
            input: 'Admite letras y numeros minimo 10, maximo 255 caracteres.'
          }
      }

    });

}
