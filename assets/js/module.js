$(function(){

  /*
  *
  */
  $('#saved_form_modules').on('click', function(e){
    e.preventDefault();
    mod_parent = $('#mod_parent').val();
    mod_name = $('#mod_name').val();
    mod_description = $('#mod_description').val();
    mod_slug = $('#mod_slug').val();

    data_module = {
      mod_parent : mod_parent,
      mod_name : mod_name,
      mod_description : mod_description,
      mod_slug : mod_slug
    };

    data_module = JSON.stringify(data_module);

    createModule(data_module);

  });

  /*
  *
  */
  $('#saved_update_form_modules').on('click', function(e){

    e.preventDefault();
    mod_id = $('#mod_id').val();
    mod_parent = $('#mod_parent').val();
    mod_name = $('#mod_name').val();
    mod_description = $('#mod_description').val();
    mod_slug = $('#mod_slug').val();

    data_module = {
      mod_id : mod_id,
      mod_parent : mod_parent,
      mod_name : mod_name,
      mod_description : mod_description,
      mod_slug : mod_slug
    };

    data_module = JSON.stringify(data_module);
    updateModule(data_module);

  });

});

/*
*
*/

function createModule(data_module){

  $.ajax({

    method: 'POST',
    url: 'registrar-modulo',
    data: {action: 'createModule', data: data_module},
    beforeSend: function(){

      $('#saved_form_modules').prop('disabled', true);
      $('#saved_form_modules').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

    }
  }).done(function(response){

    code = response.respon_code;
    msg = response.respon_msg;

    if(code == 1){
      window.setTimeout( function(){ window.location = '/editar-modulo/'+response.mod_id+'/2/1'; }, 1000);
    }

  }).fail(function(response){


});

}

/*
*
*/

function updateModule(data_module){

  $.ajax({

    method: 'POST',
    url: '/edicion-modulo',
    data: {action: 'updateModule', data: data_module},
    beforeSend: function(){

      $('#saved_update_form_modules').prop('disabled', true);
      $('#saved_update_form_modules').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

    }
  }).done(function(response){

    code = response.respon_code;
    msg = response.respon_msg;

    if(code == 1){
      window.setTimeout( function(){ window.location = '/editar-modulo/'+response.mod_id+'/1/2'; }, 1000);
    }

  }).fail(function(response){


});

}
