$(function(){

  /*
  * Accion para crear permisos
  */

  $('#saved_form_permissin').on('click', function(e){
    e.preventDefault();

    mod = '';
    role_and_permissin = [];


    $('#table-form-permissin tbody tr').each(function(){

      read_val = ' ';
      create_val = ' ';
      update_val = ' ';

        mod = $(this).attr('id');

        read = $('input[read="'+mod+'"]');
        create = $('input[create="'+mod+'"]');
        update = $('input[update="'+mod+'"]');


          if(read.is(':checked')){
              read_val = read.val();
          }

          if(create.is(':checked')){
              create_val = create.val();
          }

          if(update.is(':checked')){
              update_val = update.val();
          }

          permissin = [read_val, create_val, update_val];

          item = {
            'mod' : mod,
            'permissin' : permissin
          }

          role_and_permissin.push(item);

    });

    role_and_permissin = JSON.stringify(role_and_permissin);
    name_permissin = $('#name_permissin').val();
    description_permissin = $('#description_permissin').val();

    role = {
      name : name_permissin,
      description : description_permissin,
      permissin : role_and_permissin
    };

    role = JSON.stringify(role);

      createPermissin(role);

  });

  /*
  * Accion para editar permisos
  */

  $('#saved_update_form_permissin').on('click', function(e){
    e.preventDefault();

    mod = '';
    role_and_permissin = [];


    $('#table-form-permissin tbody tr').each(function(){

      read_val = ' ';
      create_val = ' ';
      update_val = ' ';

        mod = $(this).attr('id');

        read = $('input[read="'+mod+'"]');
        create = $('input[create="'+mod+'"]');
        update = $('input[update="'+mod+'"]');


          if(read.is(':checked')){
              read_val = read.val();
          }

          if(create.is(':checked')){
              create_val = create.val();
          }

          if(update.is(':checked')){
              update_val = update.val();
          }

          permissin = [read_val, create_val, update_val];

          item = {
            'mod' : mod,
            'permissin' : permissin
          }

          role_and_permissin.push(item);

    });

    role_and_permissin = JSON.stringify(role_and_permissin);
    name_permissin = $('#name_permissin').val();
    description_permissin = $('#description_permissin').val();
    rol_id = $('#rol_id').val();

    role = {
      rol_id : rol_id,
      name : name_permissin,
      description : description_permissin,
      permissin : role_and_permissin
    };

    role = JSON.stringify(role);

      updatePermissin(role);

  });


});

/******************************************************************************************************************************************************/

/*
* Crear permisos funcion ajax
*/

function createPermissin(role){

  $.ajax({

    method: 'POST',
    url: 'registrar-permisos',
    data: {action: 'createPermissin', data: role},
    beforeSend: function(){

      $('#saved_form_permissin').prop('disabled', true);
      $('#saved_form_permissin').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

    }
  }).done(function(response){

    code = response.respon_code;
    msg = response.respon_msg;

    if(code == 1){
      window.setTimeout( function(){ window.location = '/editar-permisos/'+response.rol_id+'/2/1'; }, 1000);
    }

  }).fail(function(response){


});

}


/*
* Editar permisos funcion ajax
*/

function updatePermissin(role){

  $.ajax({

    method: 'POST',
    url: '/edicion-permisos',
    data: {action: 'updatePermissin', data: role},
    beforeSend: function(){

      $('#saved_update_form_permissin').prop('disabled', true);
      $('#saved_update_form_permissin').html('<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>&nbsp;Procesando ...');

    }
  }).done(function(response){

    code = response.respon_code;
    msg = response.respon_msg;

    if(code == 1){
      window.setTimeout( function(){ window.location = '/editar-permisos/'+response.rol_id+'/1/2'; }, 1000);
    }

  }).fail(function(response){


});

}
